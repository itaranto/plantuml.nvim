local job = require('plantuml.job')

local M = {}

---@type { [number]: boolean }
M.success_exit_codes = { [0] = true, [200] = true }

---@param content string
---@param tmp_file string
---@param dark_mode boolean
---@param format? string
---@return job.Runner
function M.create_image_runner(content, tmp_file, dark_mode, format)
  local cmd = { 'plantuml', '-pipe' }

  if dark_mode then
    table.insert(cmd, '-darkmode')
    table.insert(cmd, '-Smonochrome=reverse')
  end

  if format then
    table.insert(cmd, string.format('-t%s', format))
  end

  return job.Runner:new(cmd, M.success_exit_codes, content, tmp_file)
end

---@param content string
---@return job.Runner
function M.create_text_runner(content)
  local cmd = { 'plantuml', '-pipe', '-tutxt' }
  return job.Runner:new(cmd, M.success_exit_codes, content)
end

return M
