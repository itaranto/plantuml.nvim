local fs = require('plantuml.fs')

local M = {}

---@class job.Runner
---@field cmd string[]
---@field exit_codes { [number]: boolean }
---@field stdin_content? string
---@field stdout_file? string
M.Runner = {}

---@param cmd string[]
---@param exit_codes? { [number]: boolean }
---@param stdin_content? string
---@param stdout_file? string
---@return job.Runner
function M.Runner:new(cmd, exit_codes, stdin_content, stdout_file)
  self.__index = self
  return setmetatable({
    cmd = cmd,
    exit_codes = exit_codes or { [0] = true },
    stdin_content = stdin_content,
    stdout_file = stdout_file,
  }, self)
end

---@param on_success? fun(stdout: string): nil
---@return number
function M.Runner:run(on_success)
  local obj = vim.system(
    self.cmd,
    { stdin = self.stdin_content },
    function(comp)
      assert(
        next(self.exit_codes) ~= nil and self.exit_codes[comp.code],
        string.format(
          'wait job for command "%s"\n%s\ncode: %d',
          table.concat(self.cmd, ' '),
          comp.stderr,
          comp.code
        )
      )

      if comp.stdout then
        if self.stdout_file then
          fs.write_file(self.stdout_file, comp.stdout)
        end

        if on_success then
          vim.schedule(function() on_success(comp.stdout) end)
        end
      end
    end)

  return obj.pid
end

return M
