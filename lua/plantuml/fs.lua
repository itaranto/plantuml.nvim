local M = {}

---@param path string
---@param data string
---@return nil
function M.write_file(path, data)
  local file, err = io.open(path, 'wb')
  assert(file, err)

  local ok, err = file:write(data)
  assert(ok, err)

  file:close()
end

return M
